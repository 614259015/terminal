<?php defined('BASEPATH') or exit('No direct script access allowed');
class Model_register extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Alumni_ad1($data1)
    {
        $this->db->insert('departure_station', $data1);
    }

    public function Alumni_ad2($data2)
    {
        $this->db->insert('huai_rat_station', $data2);
    }
    public function Alumni_ad3($data3)
    {
        $this->db->insert('terminal', $data3);
    }
    public function login($email, $password)
    {
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $result = $this->db->get('user');
        $message = '';
        if ($result->num_rows() > 0) {
            $message = true;

            // $this->load->view('list');

        } else {
            $message = false;
        }
        $data = array(
            "message" => $message, "data" => $result->result(),

        );
        return $data;
    }

    public function showuser()
    {
        $query = $this->db->get('departure_station');
        return $query->result();
        /* $query = $this->db->get('user')->result();
    $query1 = $this->db->get('professor')->result();
    $query2 = $this->db->get('seniors')->result();
    return array_merge($query,$query1,$query2); */
    }

    public function showinfo()
    {
        $this->db->select('*');
        $this->db->from('terminal');
        $this->db->join('departure_station', 'departure_station.id = terminal.id', 'left');
        //$this->db->where('terminal.T_id');
        $query = $this->db->get();
        return $query->result();
    }
    public function showinfo2()
    {
        $this->db->select('*');
        $this->db->from('departure_station');
        $this->db->join('huai_rat_station', 'huai_rat_station.id = departure_station.id', 'left');
        $this->db->join('terminal', ' terminal.id = departure_station.id ', 'left');

        //$this->db->where('terminal.T_id');
        $query = $this->db->get();
        return $query->result();
    }

    public function show_search($search)
    {
        $this->db->like('departure_station' . "." . 'T_id', $search);
        $this->db->or_like('departure_station' . "." . 'station', $search);
        $this->db->join('huai_rat_station', 'huai_rat_station.id = departure_station.id');
        $this->db->join('terminal', 'terminal.id = departure_station.id');
        $result = $this->db->get('departure_station');

        return $result->result();
    }
}
