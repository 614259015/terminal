<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *    - or -
     *         http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session', 'database');
        $this->load->model('Model_register', 'Alumni');

    }
    public function index()
    {
        $this->load->view('Home');
    }
    public function showall()
    {
        $data['query'] = $this->Alumni->showuser();
        $this->load->view('Home', $data);
    }
    public function info()
    {
        $a = $this->input->post('T_id');
        $data['re'] = $this->Alumni->showinfo();
        $this->load->view('show', $data);
    }
    public function info1()
    {
        $a = $this->input->post('id');
        $data['re'] = $this->Alumni->showinfo2();
        $this->load->view('table', $data);
    }
    public function page_register()
    {
        $this->load->view('bootstap');
        $this->load->view('header');
        $this->load->view('register');

    }

    public function Register()
    {
        $data1 = array(

            'T_id' => $this->input->post("T_id"),
            'station' => $this->input->post("station"),
            'time' => $this->input->post("time"),
            'category' => $this->input->post("category"),
        );
        $data2 = array(

            'H_park_time' => $this->input->post("H_park_time"),
            'H_ster_time' => $this->input->post("H_ster_time"),
        );
        $data3 = array(

            'T_station' => $this->input->post("T_station"),
            'T_park_time' => $this->input->post("T_park_time"),
        );

        $this->Alumni->Alumni_ad1($data1);
        $this->Alumni->Alumni_ad2($data2);
        $this->Alumni->Alumni_ad3($data3);

        /*$data['query'] = $this->Alumni->showuser();
        $this->load->view('Home', $data);*/
        redirect("Welcome/showall");

    }
    public function search()
    {
        $id = $this->input->post("search");
        $data['se'] = $this->Alumni->show_search($id);
        $this->load->view("table_se", $data);
    }

};
