<?php defined('BASEPATH') or exit('No direct script access allowed');
      $this->load->view('bootstap');
      $this->load->view('header');
        
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<div class="container">
<br>
<nav aria-label="breadcrumb">
					<ol class="breadcrumb SpaceStyle" style="background-color: #111111">
						<li class="breadcrumb-item active" aria-current="page">ตารางรถไฟ</li>
            
            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            <div class="row" >
                                <form method="post" action="<?php echo site_url('Welcome/search'); ?>">
                                    <div class="row" >
                                        <div class="col-9">
                                            <input class="form-control" type="text" placeholder="ค้นหาโดยใช้ รหัส" name="search" />
                                        </div>
                                        <div class="col-3">
                                            <input type="submit" class="btn btn-block btn-primary" style="width:100px" name="submit" value="ค้นหา">
                                        </div>
                                        </div>
                                </form>
              </div>
                           
					</ol>
          
				</nav>

  <div class="row">
  
  <?php foreach ($re as $x){ ?>
    
     <div class="col-sm"><div class="card" style="width: 30rem;">
    
  <div class="card-body">
    <h4 class="card-title" >
    รหัสรถ : <?php echo $x->T_id;?>
    <br>ชื่อสถานี : <?php echo $x->T_station;?>
    <br>เวลาที่ถึง : <?php echo $x->T_park_time;?>
    <br>ประเภทรถ : <?php echo $x->category;?>
    </h4>
    <p align="center"><a  class="btn btn-success" href="<?php echo site_url('Welcome/showall'); ?>" >กลับไปที่ต้นทาง</a></p>
</div></div><br>
</div>


  <?php }; ?>
    
  </div>
</div>

</div>

</body>
<?php $this->load->view('footer1'); ?>
</html>