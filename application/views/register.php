<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register Page</title>

    <link rel="stylesheet" href="style1.css">

</head>
<body>
<br>

    <div class="container" align="center">
<div class="card border-success mb-3" style="max-width: 45rem;">
  <div class="bg-success"><h2>เพิ่มรอบ</h2></div>
  <div class="card-body text-success">
    <p class="card-text">
    <form action="Register" method="post">
  <div class="form-group">
    <label for="H_id">รหัสรถไฟ</label>
    <input class="form-control" type="text" name="T_id" placeholder="T_id" required>
  </div>
  <div class="form-group">
    <label for="station">สถานี</label>
    <input class="form-control" type="text" name="station" placeholder="station" required>
  </div>
  <div class="form-group">
    <label for="time">เวลาออก</label>
    <input class="form-control" type="time" name="time" placeholder="time" required>
  </div>
  <div class="form-group">
    <label for="category">ประเภทรถ</label>
    <input class="form-control" type="text" name="category"  placeholder="category" required>
  </div>
  <div class="form-group">
    <label for="H_park_time">ถึงห้วยราช</label>
    <input class="form-control" type="time" name="H_park_time" placeholder="H_park_time" required>
  </div>
  <div class="form-group">
    <label for="H_ster_time">ออกจากห้วยราช</label><br>
    <input type="time" class="form-control" name="H_ster_time"  placeholder="H_ster_time" required>
  </div>
  <div class="form-group">
    <label for="T_station">สถานีปลายทาง</label><br>
    <input type="text" name="T_station" class="form-control"  rows="3"   placeholder="T_station" required>
  </div>
  <div class="form-group">
    <label for="T_park_time">เวลาที่ถึง</label><br>
    <input class="form-control" type="time" name="T_park_time"  placeholder="T_park_time" required>
  </div>
  <button type="submit" name="submit" class="btn btn-success"> Regiter</button>
  <br><br>
</form>
  </div>
  <div class="card-footer bg-success"></div>
</div>
</div>


</body>
</html>
