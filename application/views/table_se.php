<?php defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('bootstap');
$this->load->view('header');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<div class="container">
<br>
<nav aria-label="breadcrumb">
					<ol class="breadcrumb SpaceStyle" style="background-color: #111111">
						<li class="breadcrumb-item active" aria-current="page">ตารางรถไฟ</li>

            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            <div class="row" >
                                <form method="post" action="<?php echo site_url('Welcome/search'); ?>">
                                    <div class="row" >
                                        <div class="col-9">
                                            <input class="form-control" type="text" placeholder="ค้นหาโดยใช้ รหัส" name="search" />
                                        </div>
                                        <div class="col-3">
                                            <input type="submit" class="btn btn-block btn-primary" style="width:100px" name="submit" value="ค้นหา">
                                        </div>
                                        </div>
                                </form>
              </div>

					</ol>

				</nav>

                <table class="table">
                <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">ต้นทาง</th>
                    <th scope="col"></th>
                    <th scope="col">ห้วยราช</th>
                    <th scope="col"></th>
                    <th scope="col">ปลายทาง</th>
                    <th scope="col"></th>
                    <th scope="col">หมายเหตุ</th>
                    <th scope="col"></th>
                    </tr>
            </thead>
            <thead>
                <tr>
                    <th scope="col">รหัสรถไฟ</th>
                    <th scope="col">สถานีต้นทาง</th>
                    <th scope="col">เวลาออก</th>
                    <th scope="col">ถึงห้วยราช</th>
                    <th scope="col">ออกจากห้วยราช</th>
                    <th scope="col">สถานีปลายทาง</th>
                    <th scope="col">เวลาที่ถึง</th>
                    <th scope="col">ประเภทรถ</th>
                    <th scope="col">ดูปลายทาง</th>

                    </tr>
            </thead>


  <?php foreach ($se as $x) {?>

    <tbody>
                    <tr>
                        <th scope="row"> <?php echo $x->T_id; ?> </th>
                        <th scope="row"> <?php echo $x->station; ?> </th>
                        <th scope="row"> <?php echo $x->time; ?> </th>
                        <th scope="row"> <?php echo $x->H_park_time; ?> </th>
                        <th scope="row"> <?php echo $x->H_ster_time; ?> </th>
                        <th scope="row"> <?php echo $x->T_station; ?> </th>
                        <th scope="row"> <?php echo $x->T_park_time; ?> </th>
                        <th scope="row"> <?php echo $x->category; ?> </th>
                        <td>
                        <form action="./info" method="POST">
    <input type="text" name="T_id" value="<?php echo $x->T_id; ?>" hidden>
    <input type="submit" class="btn btn-success" name="submit" value="ปลายทาง"></form></a>
    </form>

                        </td>
                        <!-- <td><a href=""><button type="button" class="btn btn-danger">ลบ</button></a></td> -->

                    </tr>
                </tbody>
  <?php }
;?>



</div>

</body>

</html>