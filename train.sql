-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 05, 2020 at 03:46 AM
-- Server version: 8.0.17
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `train`
--

-- --------------------------------------------------------

--
-- Table structure for table `departure_station`
--

CREATE TABLE `departure_station` (
  `id` varchar(3) NOT NULL,
  `H_id` varchar(3) NOT NULL,
  `station` varchar(100) NOT NULL,
  `time` time NOT NULL,
  `category` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Dumping data for table `departure_station`
--

INSERT INTO `departure_station` (`id`, `H_id`, `station`, `time`, `category`) VALUES
('234', '001', 'สุรินทร์', '05:20:00', 'รถธรรมดา'),
('424', '002', 'สำโรงทาบ', '05:50:00', 'รถดีเซลราง'),
('428', '004', 'อุบลราาชธานี', '06:20:00', 'รถดีเซลราง'),
('72', '003', 'อุบลราาชธานี', '05:40:00', 'รถด่วนดีเซลราง');

-- --------------------------------------------------------

--
-- Table structure for table `huai_rat_station`
--

CREATE TABLE `huai_rat_station` (
  `id` varchar(3) NOT NULL,
  `H_park_time` time NOT NULL,
  `H_ster_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Dumping data for table `huai_rat_station`
--

INSERT INTO `huai_rat_station` (`id`, `H_park_time`, `H_ster_time`) VALUES
('001', '05:53:00', '05:54:00'),
('002', '07:39:00', '07:40:00'),
('003', '08:22:00', '08:23:00'),
('004', '09:38:00', '09:39:00');

-- --------------------------------------------------------

--
-- Table structure for table `terminal`
--

CREATE TABLE `terminal` (
  `T_id` varchar(3) NOT NULL,
  `T_station` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `T_park_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Dumping data for table `terminal`
--

INSERT INTO `terminal` (`T_id`, `T_station`, `T_park_time`) VALUES
('234', 'กรุงเทพ', '14:15:00'),
('424', 'นครราชสีมา', '09:50:00'),
('428', 'นครราชสีมา', '14:55:00'),
('72', 'กรุงเทพ', '11:45:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departure station`
--
ALTER TABLE `departure_station`
  ADD PRIMARY KEY (`id`),
  ADD KEY `H_id` (`H_id`);

--
-- Indexes for table `huai rat station`
--
ALTER TABLE `huai_rat_station`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terminal`
--
ALTER TABLE `terminal`
  ADD PRIMARY KEY (`T_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `departure station`
--
ALTER TABLE `departure_station`
  ADD CONSTRAINT `departure_station_ibfk_2` FOREIGN KEY (`id`) REFERENCES `terminal` (`T_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `departure_station_ibfk_3` FOREIGN KEY (`H_id`) REFERENCES `huai_rat_station` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
